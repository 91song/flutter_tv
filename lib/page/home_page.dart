import 'dart:async';
import 'dart:math';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tv/entity/nav_entity.dart';
import 'package:flutter_tv/entity/up_entity.dart';
import 'package:flutter_tv/generated/l10n.dart';
import 'package:logger/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:player/player.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:volume_watcher/volume_watcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  Logger logger = Logger();
  late StreamSubscription subscription;
  double topHeight = 90.h;
  bool upDownShake = false;
  List<UpEntity> upEntities = [];
  List<NavEntity> navEntities = [];
  late AnimationController shakeAnimationController;
  late Animation<double> shakeAnimation;
  late AnimationController upAnimationController;
  late Animation<Offset> upSlideAnimation;
  late Animation<double> upFadeAnimation;
  late AutoScrollController navScrollController;
  late PageController pageController;
  late ScrollController contentScrollController;
  late AnimationController scaleAnimationController;
  late Animation<double> scaleAnimation;
  late FocusNode upFirstFocusNode = FocusNode();
  late FocusNode navFirstFocusNode = FocusNode();
  late FocusNode upLastFocusNode = upFirstFocusNode;
  late FocusNode navLastFocusNode = navFirstFocusNode;
  int currentIndex = 0;
  Player player = Player();
  EventChannel eventChannel = const EventChannel('me.victor.player_event');

  @override
  void initState() {
    super.initState();
    subscription = Connectivity().onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none) {
        logger.d('网络已断开');
      } else {
        logger.d('网络已连接');
      }
    });
    upEntities.add(UpEntity()
      ..title = '搜索'
      ..image = 'images/search.png'
      ..imageFocused = 'images/search_focused.png');
    upEntities.add(UpEntity()
      ..title = '消息'
      ..image = 'images/message.png'
      ..imageFocused = 'images/search_focused.png');
    upEntities.add(UpEntity()
      ..title = '直播'
      ..image = 'images/live.png'
      ..imageFocused = 'images/search_focused.png');
    upEntities.add(UpEntity()
      ..title = '我的'
      ..image = 'images/my.png'
      ..imageFocused = 'images/search_focused.png');
    upEntities.add(UpEntity()
      ..title = '应用商城'
      ..image = 'images/market.png'
      ..imageFocused = 'images/search_focused.png');
    upEntities.add(UpEntity()
      ..title = '切换模式'
      ..image = 'images/switch.png'
      ..imageFocused = 'images/switch_focused.png');
    upEntities.add(UpEntity()
      ..title = '历史记录'
      ..image = 'images/history.png'
      ..imageFocused = 'images/search_focused.png');
    navEntities.add(NavEntity()..name = '央视');
    navEntities.add(NavEntity()..name = '直播');
    navEntities.add(NavEntity()
      ..name = '推荐'
      ..defaultFocused = true);
    navEntities.add(NavEntity()..name = '电影');
    navEntities.add(NavEntity()..name = '电视剧');
    navEntities.add(NavEntity()..name = '综艺');
    navEntities.add(NavEntity()..name = '少儿');
    navEntities.add(NavEntity()..name = '纪录片');
    navEntities.add(NavEntity()..name = '复兴之路');
    navEntities.add(NavEntity()..name = '教育');
    navEntities.add(NavEntity()..name = '游戏');
    navEntities.add(NavEntity()..name = '云游戏');
    navEntities.add(NavEntity()..name = '电竞');
    navEntities.add(NavEntity()..name = '健康');
    navEntities.add(NavEntity()..name = '音乐');
    navEntities.add(NavEntity()..name = '聚精彩');
    navEntities.add(NavEntity()..name = '新闻');
    navEntities.add(NavEntity()..name = '本地');
    navEntities.add(NavEntity()..name = '体育');
    navEntities.add(NavEntity()..name = '翼商城');
    navEntities.add(NavEntity()..name = '财经');
    shakeAnimationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    shakeAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: shakeAnimationController, curve: Curves.bounceInOut));
    upAnimationController = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    upSlideAnimation =
        Tween(begin: const Offset(0, 0), end: const Offset(0, -1)).animate(
            CurvedAnimation(
                parent: upAnimationController, curve: Curves.decelerate));
    upFadeAnimation = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
        parent: upAnimationController, curve: Curves.decelerate));
    navScrollController = AutoScrollController();
    pageController = PageController();
    contentScrollController = ScrollController();
    contentScrollController.addListener(() {
      if (contentScrollController.offset == 0) {
        if (upFadeAnimation.value == 0.0 &&
            (upAnimationController.isCompleted ||
                upAnimationController.isDismissed)) {
          upAnimationController.reverse();
        }
      } else {
        if (upFadeAnimation.value == 1.0 &&
            (upAnimationController.isCompleted ||
                upAnimationController.isDismissed)) {
          upAnimationController.forward();
        }
      }
    });
    scaleAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    scaleAnimation = Tween(begin: 1.0, end: 1.1).animate(CurvedAnimation(
        parent: scaleAnimationController, curve: Curves.easeInOut));
    eventChannel.receiveBroadcastStream().listen((event) {
      logger.d(event);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF0A0A0A),
                Color(0xFF1F1F1F),
                Color(0xFF0A0A0A),
              ],
            ),
          ),
        ),
        SizedBox(
          child: PageView.builder(
            itemCount: navEntities.length,
            controller: pageController,
            itemBuilder: (context, index) {
              return getPageWidget(index);
            },
          ),
        ),
        SlideTransition(
          position: upSlideAnimation,
          child: FadeTransition(
            opacity: upFadeAnimation,
            child: SizedBox(
              child: Column(
                children: [
                  Container(
                    height: topHeight,
                    padding: EdgeInsets.symmetric(
                      horizontal: 84.h,
                    ),
                    color: const Color(
                      0x1FFFFFFF,
                    ),
                    child: Row(
                      children: getUpWidgets(),
                    ),
                  ),
                  Container(
                    height: 61.8.h,
                    margin: EdgeInsets.symmetric(
                      horizontal: 49.5.h,
                      vertical: 19.1.h,
                    ),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      clipBehavior: Clip.none,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: navEntities.length,
                      controller: navScrollController,
                      itemBuilder: (BuildContext context, int index) {
                        return getNavWidget(index);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: const Color(0x99000000),
        items: [
          BottomNavigationBarItem(
              icon: Image.asset(width: 40.h, height: 40.h, 'images/search.png'),
              activeIcon: Image.asset(
                  width: 40.h, height: 40.h, 'images/search_focused.png'),
              label: 'Home'),
          BottomNavigationBarItem(
              icon: Image.asset(width: 40.h, height: 40.h, 'images/search.png'),
              activeIcon: Image.asset(
                  width: 40.h, height: 40.h, 'images/search_focused.png'),
              label: 'Home'),
          BottomNavigationBarItem(
              icon: Image.asset(width: 40.h, height: 40.h, 'images/search.png'),
              activeIcon: Image.asset(
                  width: 40.h, height: 40.h, 'images/search_focused.png'),
              label: 'Home'),
          BottomNavigationBarItem(
              icon: Image.asset(width: 40.h, height: 40.h, 'images/search.png'),
              activeIcon: Image.asset(
                  width: 40.h, height: 40.h, 'images/search_focused.png'),
              label: 'Home'),
        ],
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.red,
      ),
    );
  }

  getPlatformVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    logger.d(packageInfo.appName);
    logger.d(packageInfo.version);
    logger.d(packageInfo.buildNumber);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('name', 'Victor');
    final String? name = prefs.getString('name');
    await player.showToast(name ?? 'Null');
    AndroidDeviceInfo deviceInfo = await DeviceInfoPlugin().androidInfo;
    logger.d(deviceInfo.model);
    String? platformVersion = await player.getPlatformVersion();
    logger.d(platformVersion);
    await player.showToast(S.current.greet('成都'));
    double maxVolume = await VolumeWatcher.getMaxVolume;
    double currentVolume = await VolumeWatcher.getCurrentVolume;
    logger.d(maxVolume);
    logger.d(currentVolume);
    await VolumeWatcher.setVolume(maxVolume / 2);
  }

  requestPermission() async {
    PermissionStatus status = await Permission.camera.status;
    if (status.isDenied) {
      if (await Permission.camera.request().isGranted) {
        await player.showToast('相机权限申请允许');
      } else {
        await player.showToast('相机权限申请拒绝');
      }
    } else {
      await player.showToast('相机权限申请允许');
    }
    ConnectivityResult result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile) {
      logger.d('手机网络');
    } else if (result == ConnectivityResult.wifi) {
      logger.d('WIFI网络');
    }
  }

  loadUrl() async {
    if (await canLaunchUrl(Uri.parse('https://flutter.dev'))) {
      await launchUrl(Uri.parse('https://flutter.dev'));
    }
  }

  List<Widget> getUpWidgets() {
    List<Widget> widgets = [];
    for (int i = 0; i < upEntities.length; i++) {
      UpEntity upEntity = upEntities[i];
      Widget widget = Container(
        height: upEntity.focused ? 64.h : 36.h,
        margin: EdgeInsets.only(right: 48.h),
        padding: EdgeInsets.symmetric(
          horizontal: upEntity.focused ? 21.h : 0,
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: upEntity.focused
                ? const Color(
                    0x1AFFFFFF,
                  )
                : Colors.transparent,
            width: 4.h,
            strokeAlign: StrokeAlign.outside,
          ),
          color: upEntity.focused
              ? const Color(
                  0x99FFFFFF,
                )
              : Colors.transparent,
          borderRadius: BorderRadius.circular((36.h)),
        ),
        child: Row(
          children: [
            Image(
              width: 36.h,
              height: 36.h,
              image: AssetImage(
                upEntity.focused ? upEntity.imageFocused! : upEntity.image!,
              ),
            ),
            SizedBox(
              width: upEntity.focused ? 16.h : 0,
            ),
            Visibility(
              visible: upEntity.focused,
              child: Text(
                upEntity.title!,
                style: TextStyle(
                  fontSize: 36.sp,
                  color: const Color(
                    0xFF34322D,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
      widgets.add(
        Focus(
          focusNode: i == 0 ? upFirstFocusNode : null,
          onKeyEvent: (node, event) {
            if (event.runtimeType == KeyDownEvent ||
                event.runtimeType == KeyRepeatEvent) {
              if (event.physicalKey == PhysicalKeyboardKey.enter) {
                Navigator.of(context).pushNamed('/Setting');
                getPlatformVersion();
                // requestPermission();
                // Fluttertoast.showToast(
                //     msg: '谢谢惠顾',
                //     backgroundColor: const Color(0xFFFFFFFF),
                //     textColor: const Color(0xFF000000),
                //     toastLength: Toast.LENGTH_LONG,
                //     gravity: ToastGravity.CENTER);
                // logger.d(format.formatDate(DateTime.now(),
                //     [format.yyyy, '-', format.mm, '-', format.dd]));
                // String jsonData = '{"title": "推荐", "image": "xxx.png"}';
                // UpEntity upEntity = UpEntity.fromJson(json.decode(jsonData));
                // logger.d(upEntity.title);
                // logger.d(upEntity.image);
                // loadUrl();
                // AutoOrientation.landscapeLeftMode();
                return KeyEventResult.handled;
              } else if (event.physicalKey == PhysicalKeyboardKey.arrowUp) {
                setState(() {
                  upDownShake = true;
                });
                if (shakeAnimationController.isCompleted ||
                    shakeAnimationController.isDismissed) {
                  shakeAnimationController.reset();
                  shakeAnimationController.forward();
                }
                return KeyEventResult.handled;
              } else if (event.physicalKey == PhysicalKeyboardKey.arrowDown) {
                upLastFocusNode = node;
                FocusScope.of(context).requestFocus(navLastFocusNode);
                return KeyEventResult.handled;
              } else if (event.physicalKey == PhysicalKeyboardKey.arrowLeft) {
                if (i == 0) {
                  setState(() {
                    upDownShake = false;
                  });
                  if (shakeAnimationController.isCompleted ||
                      shakeAnimationController.isDismissed) {
                    shakeAnimationController.reset();
                    shakeAnimationController.forward();
                  }
                  return KeyEventResult.handled;
                }
              } else if (event.physicalKey == PhysicalKeyboardKey.arrowRight) {
                if (i == upEntities.length - 1) {
                  setState(() {
                    upDownShake = false;
                  });
                  upLastFocusNode = node;
                  FocusScope.of(context).requestFocus(navLastFocusNode);
                  return KeyEventResult.handled;
                }
              }
            }
            return KeyEventResult.ignored;
          },
          onFocusChange: (focus) {
            setState(() {
              topHeight = focus ? 150.h : 90.h;
              upEntity.focused = focus;
            });
            if (focus) {
              shakeAnimationController.reset();
            }
          },
          child: upEntity.focused
              ? AnimatedBuilder(
                  animation: shakeAnimation,
                  builder: (context, child) {
                    double shake =
                        sin(3 * 2 * pi * shakeAnimationController.value) * 5.h;
                    return Transform.translate(
                      offset: upDownShake ? Offset(0, shake) : Offset(shake, 0),
                      child: child,
                    );
                  },
                  child: widget,
                )
              : widget,
        ),
      );
    }
    return widgets;
  }

  Widget getNavWidget(int index) {
    NavEntity navEntity = navEntities[index];
    Widget widget = Container(
      margin: EdgeInsets.only(right: index < navEntities.length - 1 ? 19.h : 0),
      padding: EdgeInsets.symmetric(
        horizontal: 34.5.h,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: navEntity.focused
              ? const Color(
                  0x1AFFFFFF,
                )
              : Colors.transparent,
          width: 4.h,
          strokeAlign: StrokeAlign.outside,
        ),
        color: navEntity.focused
            ? const Color(
                0x99FFFFFF,
              )
            : Colors.transparent,
        borderRadius: BorderRadius.circular((34.h)),
      ),
      child: Center(
        child: Text(
          navEntity.name!,
          style: TextStyle(
            fontSize: 36.sp,
            color: Color(
              navEntity.focused ? 0xFF34322D : 0xFFCCCCCC,
            ),
          ),
        ),
      ),
    );
    return AutoScrollTag(
      key: ValueKey(index),
      controller: navScrollController,
      index: index,
      child: Focus(
        autofocus: navEntities[index].defaultFocused,
        focusNode: navEntities[index].defaultFocused ? navFirstFocusNode : null,
        onKeyEvent: (node, event) {
          if (event.runtimeType == KeyDownEvent ||
              event.runtimeType == KeyRepeatEvent) {
            if (event.physicalKey == PhysicalKeyboardKey.arrowUp) {
              navLastFocusNode = node;
              FocusScope.of(context).requestFocus(upLastFocusNode);
              return KeyEventResult.handled;
            } else if (event.physicalKey == PhysicalKeyboardKey.arrowDown) {
              navLastFocusNode = node;
            } else if (event.physicalKey == PhysicalKeyboardKey.arrowLeft) {
              if (index == 0) {
                setState(() {
                  upDownShake = false;
                });
                if (shakeAnimationController.isCompleted ||
                    shakeAnimationController.isDismissed) {
                  shakeAnimationController.reset();
                  shakeAnimationController.forward();
                }
                return KeyEventResult.handled;
              }
            } else if (event.physicalKey == PhysicalKeyboardKey.arrowRight) {
              if (index == navEntities.length - 1) {
                setState(() {
                  upDownShake = false;
                });
                if (shakeAnimationController.isCompleted ||
                    shakeAnimationController.isDismissed) {
                  shakeAnimationController.reset();
                  shakeAnimationController.forward();
                }
                return KeyEventResult.handled;
              } else {
                FocusScope.of(context)
                    .focusInDirection(TraversalDirection.right);
                return KeyEventResult.handled;
              }
            }
          }
          return KeyEventResult.ignored;
        },
        onFocusChange: (focus) {
          setState(() {
            navEntities[index].focused = focus;
          });
          if (focus) {
            shakeAnimationController.reset();
            scaleAnimationController.reset();
            scaleAnimationController.forward();
            pageController.animateToPage(index,
                duration: const Duration(milliseconds: 300),
                curve: Curves.linear);
            if (!navScrollController.isAutoScrolling) {
              navScrollController.scrollToIndex(index,
                  preferPosition: AutoScrollPosition.middle);
            }
          }
        },
        child: navEntity.focused
            ? ScaleTransition(
                scale: scaleAnimation,
                child: AnimatedBuilder(
                  animation: shakeAnimation,
                  builder: (context, child) {
                    double shake =
                        sin(3 * 2 * pi * shakeAnimationController.value) * 5.h;
                    return Transform.translate(
                      offset: upDownShake ? Offset(0, shake) : Offset(shake, 0),
                      child: child,
                    );
                  },
                  child: widget,
                ),
              )
            : widget,
      ),
    );
  }

  Widget getPageWidget(int index) {
    return MediaQuery.removePadding(
      removeTop: true,
      context: context,
      child: Transform.translate(
        offset: Offset(
          0,
          topHeight == 150.h ? 60.h : 0,
        ),
        child: ListView.builder(
          itemCount: 10,
          controller: contentScrollController,
          itemBuilder: (BuildContext context, int index) {
            return Focus(
              onKeyEvent: (node, event) {
                if (event.runtimeType == KeyDownEvent ||
                    event.runtimeType == KeyRepeatEvent) {
                  if (event.physicalKey == PhysicalKeyboardKey.arrowUp) {
                    if (index == 0) {
                      FocusScope.of(context).requestFocus(navLastFocusNode);
                      return KeyEventResult.handled;
                    }
                  }
                }
                return KeyEventResult.ignored;
              },
              child: Container(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.black
                    : Colors.white,
                height: 200.h,
                margin: EdgeInsets.only(
                  top: index == 0 ? 203.h : 0,
                  bottom: 48.h,
                ),
                child: const Center(
                    child: Icon(
                  Icons.thumb_up,
                  size: 30,
                )),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  dispose() {
    subscription.cancel();
    shakeAnimationController.dispose();
    upAnimationController.dispose();
    scaleAnimationController.dispose();
    navScrollController.dispose();
    pageController.dispose();
    contentScrollController.dispose();
    super.dispose();
  }
}
