import 'package:flutter_tv/generated/json/base/json_convert_content.dart';
import 'package:flutter_tv/entity/up_entity.dart';

UpEntity $UpEntityFromJson(Map<String, dynamic> json) {
	final UpEntity upEntity = UpEntity();
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		upEntity.title = title;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		upEntity.image = image;
	}
	final String? imageFocused = jsonConvert.convert<String>(json['imageFocused']);
	if (imageFocused != null) {
		upEntity.imageFocused = imageFocused;
	}
	final bool? focused = jsonConvert.convert<bool>(json['focused']);
	if (focused != null) {
		upEntity.focused = focused;
	}
	return upEntity;
}

Map<String, dynamic> $UpEntityToJson(UpEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['title'] = entity.title;
	data['image'] = entity.image;
	data['imageFocused'] = entity.imageFocused;
	data['focused'] = entity.focused;
	return data;
}