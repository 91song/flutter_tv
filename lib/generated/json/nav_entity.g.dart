import 'package:flutter_tv/generated/json/base/json_convert_content.dart';
import 'package:flutter_tv/entity/nav_entity.dart';

NavEntity $NavEntityFromJson(Map<String, dynamic> json) {
	final NavEntity navEntity = NavEntity();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		navEntity.name = name;
	}
	final bool? focused = jsonConvert.convert<bool>(json['focused']);
	if (focused != null) {
		navEntity.focused = focused;
	}
	final bool? active = jsonConvert.convert<bool>(json['active']);
	if (active != null) {
		navEntity.active = active;
	}
	final bool? defaultFocused = jsonConvert.convert<bool>(json['defaultFocused']);
	if (defaultFocused != null) {
		navEntity.defaultFocused = defaultFocused;
	}
	return navEntity;
}

Map<String, dynamic> $NavEntityToJson(NavEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	data['focused'] = entity.focused;
	data['active'] = entity.active;
	data['defaultFocused'] = entity.defaultFocused;
	return data;
}