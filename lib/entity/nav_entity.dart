import 'package:flutter_tv/generated/json/base/json_field.dart';
import 'package:flutter_tv/generated/json/nav_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class NavEntity {
  String? name;
  bool focused = false;
  bool active = false;
  bool defaultFocused = false;

  NavEntity();

  factory NavEntity.fromJson(Map<String, dynamic> json) =>
      $NavEntityFromJson(json);

  Map<String, dynamic> toJson() => $NavEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
