import 'package:flutter_tv/generated/json/base/json_field.dart';
import 'package:flutter_tv/generated/json/up_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class UpEntity {
  String? title;
  String? image;
  String? imageFocused;
  bool focused = false;

  UpEntity();

  factory UpEntity.fromJson(Map<String, dynamic> json) =>
      $UpEntityFromJson(json);

  Map<String, dynamic> toJson() => $UpEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
